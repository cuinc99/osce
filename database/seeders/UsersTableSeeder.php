<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'password'       => bcrypt('password'),
                'remember_token' => null,
                'username'            => 'admin',
                'login_at'            => now(),
            ],
        ];

        User::insert($users);
    }
}
