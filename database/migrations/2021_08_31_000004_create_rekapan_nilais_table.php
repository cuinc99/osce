<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekapanNilaisTable extends Migration
{
    public function up()
    {
        Schema::create('rekapan_nilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('semester');
            $table->float('nilai_angka', 15, 2)->nullable();
            $table->string('nilai_huruf')->nullable();
            $table->integer('station_diikuti')->nullable();
            $table->integer('station_tidak_lulus')->nullable();
            $table->longText('ket_station_tidak_lulus')->nullable();
            $table->longText('ket_belum_ikut_csl')->nullable();
            $table->string('import_code')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->timestamps();
        });
    }
}
