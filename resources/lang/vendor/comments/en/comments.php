<?php

return [
    'there_are_no_comments' => 'Belum ada komentar.',
    'authentication_required' => 'Otentikasi diperlukan',
    'you_must_login_to_post_a_comment' => 'Anda harus masuk untuk mengirim komentar.',
    'log_in' => 'Log in',
    'reply' => 'Balas',
    'edit' => 'Edit',
    'delete' => 'Hapus',
    'edit_comment' => 'Edit komentar',
    'update_your_message_here' => 'Perbarui pesan Anda di sini:',
    'update' => 'Ubah',
    'cancel' => 'Batal',
    'reply_to_comment' => 'Balas komentar',
    'enter_your_message_here' => 'Komentar:',
    'markdown_cheatsheet' => '<a target="_blank" href=":url">Markdown</a> cheatsheet.',
    'submit' => 'Submit',
    'your_message_is_required' => 'Pesan Anda diperlukan.',
    'enter_your_message_here' => 'Komentar:',
    'enter_your_email_here' => 'Masukkan email Anda disini:',
    'enter_your_name_here' => 'Masukkan Nama Anda disini:',
];
