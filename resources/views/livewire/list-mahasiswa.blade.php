<div class="box box-primary " style="border-top-color: #1d4350">
    <div class="box-header with-border">
        <h3 class="box-title">Data Mahasiswa</h3>
    </div>
    <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th colspan="5">
                            <input wire:model='cari' type="text" class="form-control" placeholder="Cari Ketik NIM atau Nama Mahasiswa...">
                        </th>
                    </tr>
                    <tr>
                        <th width="10">No</th>
                        <th>NIM</th>
                        <th>NAMA MAHASISWA</th>
                        <th>TOTAL REKAPAN NILAI</th>
                        <th width="10"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mahasiswas as $mahasiswa)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $mahasiswa->username ?? '-' }}</td>
                            <td>{{ $mahasiswa->name ?? '-' }}</td>
                            <td>{{ $mahasiswa->rekapanNilais->count() ?? '0' }} Semester</td>
                            <td>
                                <a href="{{ route('admin.mahasiswa.show', $mahasiswa->id) }}" class="btn btn-primary">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        {{ $mahasiswas->links() }}
    </div>
</div>
