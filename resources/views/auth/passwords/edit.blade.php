@extends('layouts.admin')
@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('global.change_password') }}
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="{{ route('profile.password.update') }}">
                            @csrf
                            <div class="form-group">
                                <label>Username/NIM</label>
                                <input class="form-control" value="{{ auth()->user()->username }}" disabled>
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input class="form-control" value="{{ auth()->user()->name }}" disabled>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="required" for="password">Password Baru</label>
                                <input class="form-control" type="password" name="password" id="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block" role="alert">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="required" for="password_confirmation">Konfirmasi Password Baru</label>
                                <input class="form-control" type="password" name="password_confirmation"
                                    id="password_confirmation" required>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-danger" type="submit">
                                    {{ trans('global.save') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
