@extends('layouts.app')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <img src="/images/logo.png" style="height: 100px; margin-bottom: 5px" alt="">
        <br>
        <a href="{{ route('admin.home') }}">
            Nilai <strong>OSCE</strong> • FK Unizar
        </a>
    </div>
    <div class="login-box-body" style="background: none">

        @if (session('message'))
            <p class="alert alert-info">
                {{ session('message') }}
            </p>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <input id="username" type="text" name="username" class="form-control input-lg" required autocomplete="username"
                    autofocus placeholder="{{ trans('global.login_username') }} / NIM"
                    value="{{ old('username', null) }}" autofocus>

                @if ($errors->has('username'))
                    <p class="help-block">
                        {{ $errors->first('username') }}
                    </p>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" name="password" class="form-control input-lg" required
                    placeholder="{{ trans('global.login_password') }}">

                @if ($errors->has('password'))
                    <p class="help-block">
                        {{ $errors->first('password') }}
                    </p>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-lg btn-block btn-flat" style="background: linear-gradient(90deg, rgba(29,67,80,1) 0%, rgba(164,57,49,1) 100%); color: white">
                        {{ trans('global.login') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
@endsection
