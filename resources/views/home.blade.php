@extends('layouts.admin')
@section('content')
    <div class="content">

        @if (auth()->user()->isAdmin)
            <div class="row">
                <div class="col-lg-3">
                    <div class="small-box bg-blue">
                        <div class="inner">
                            <h3>{{ $mahasiswas->count() }}</h3>

                            <p>Mahasiswa</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{ $rekapanNilaiCount }}</h3>

                            <p>Rekapan Nilai</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="{{ route('admin.rekapan-nilais.index') }}" class="small-box-footer">Selengkapnya <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{ $pengumumans->count() }}</h3>

                            <p>Pengumuman</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="{{ route('admin.pengumumen.index') }}" class="small-box-footer">Selengkapnya <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-md-9">
                    @livewire('list-mahasiswa')
                </div>

            </div>
        @endif

        @if (!auth()->user()->isAdmin)

            @if ($pengumumans->count() > 0)
                <div class="callout callout-info">
                    <h4>Pengumuman!</h4>
                    @foreach ($pengumumans as $pengumuman)
                        <blockquote>
                            <p>{{ $pengumuman->judul }}</p>
                            <small style="color: white">{{ $pengumuman->isi }}</small>
                        </blockquote>
                    @endforeach
                </div>
            @endif

            <div class="row">

                @foreach (auth()->user()->rekapanNilais->sortBy('semester')
        as $rekapanNilai)

                    @php
                        $array = ['success', 'info', 'primary', 'warning', 'danger'];
                        $randomColor = \HiFolks\RandoPhp\Draw::sample($array)->snap();
                    @endphp

                    <div class="col-md-3" style="margin-bottom: 30px">
                        <div class="box box-{{ $randomColor }} box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Semester {{ $rekapanNilai->semester ?? '-' }}</h3>
                            </div>
                            <div class="box-body">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Nilai Angka</b> <a
                                            class="pull-right"><span
                                                class="label bg-orange">{{ number_format($rekapanNilai->nilai_angka, 2, ',', ' ') ?? '-' }}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Nilai Huruf</b> <a
                                            class="pull-right"><span
                                                class="label bg-teal">{{ $rekapanNilai->nilai_huruf ?? '-' }}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Jumlah Station yang Diikuti</b> <a
                                            class="pull-right"><span
                                                class="label bg-maroon">{{ $rekapanNilai->station_diikuti ?? '-' }}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Jumlah Station yang Tidak Lulus</b> <a
                                            class="pull-right"><span
                                                class="label bg-aqua">{{ $rekapanNilai->station_tidak_lulus ?? '-' }}</span></a>
                                    </li>
                                </ul>

                                <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <strong>Nama Station yang Tidak Lulus</strong>

                                <p>
                                    @if ($rekapanNilai->ket_station_tidak_lulus)
                                        @php
                                            $ketstl = explode(',', $rekapanNilai->ket_station_tidak_lulus);
                                        @endphp
                                        @foreach ($ketstl as $ket)
                                            <span class="label label-danger">{{ $ket }}</span>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </p>

                                <hr>

                                <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <strong>Nama Station yang Belum Ikut CSL
                                </strong>

                                <p>
                                    @if ($rekapanNilai->ket_belum_ikut_csl)
                                        @php
                                            $ketsbl = explode(',', $rekapanNilai->ket_belum_ikut_csl);
                                        @endphp
                                        @foreach ($ketsbl as $ket)
                                            <span class="label label-warning">{{ $ket }}</span>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </p>

                                <hr>

                                @comments(['model' => $rekapanNilai])
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        @endif
    </div>
@endsection
@section('scripts')
    @parent

@endsection
