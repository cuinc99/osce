@extends('layouts.admin')
@section('content')
<div class="content">
    @can('rekapan_nilai_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                {{-- <a class="btn btn-success" href="{{ route('admin.rekapan-nilais.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.rekapanNilai.title_singular') }}
                </a> --}}
                <a class="btn bg-purple" data-toggle="collapse" data-parent="#accordion" href="#importRekapanNilai">
                    Import Rekapan Nilai
                </a>
                <div class="box box-solid collapse" id="importRekapanNilai">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Rekapan Nilai</h3>
                    </div>
                    <form action="{{ route('admin.rekapan-nilais.import') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="file">Pilih File Excel</label>
                                        <input type="file" name="file" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Import</button>
                            <a href="/template/Template Import OSCE.xlsx" class="btn btn-primary">Download Template</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.rekapanNilai.title_singular') }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover datatable datatable-RekapanNilai">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        NIM
                                    </th>
                                    <th>
                                        Nama Mahasiswa
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.semester') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.nilai_angka') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.nilai_huruf') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.station_diikuti') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.station_tidak_lulus') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.ket_station_tidak_lulus') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.ket_belum_ikut_csl') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rekapanNilais as $key => $rekapanNilai)
                                    <tr data-entry-id="{{ $rekapanNilai->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->user->username ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->user->name ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->semester ?? '-' }}
                                        </td>
                                        <td>
                                            {{ number_format($rekapanNilai->nilai_angka, 2, ',', ' ') ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->nilai_huruf ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->station_diikuti ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->station_tidak_lulus ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->ket_station_tidak_lulus ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $rekapanNilai->ket_belum_ikut_csl ?? '-' }}
                                        </td>
                                        <td>
                                            @can('rekapan_nilai_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.rekapan-nilais.show', $rekapanNilai->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan

                                            @can('rekapan_nilai_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.rekapan-nilais.edit', $rekapanNilai->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('rekapan_nilai_delete')
                                                <form action="{{ route('admin.rekapan-nilais.destroy', $rekapanNilai->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('rekapan_nilai_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.rekapan-nilais.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  let table = $('.datatable-RekapanNilai:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

})

</script>
@endsection
