@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.rekapanNilai.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.rekapan-nilais.update", [$rekapanNilai->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('semester') ? 'has-error' : '' }}">
                            <label class="required" for="semester">{{ trans('cruds.rekapanNilai.fields.semester') }}</label>
                            <input class="form-control" type="number" name="semester" id="semester" value="{{ old('semester', $rekapanNilai->semester) }}" step="1" required>
                            @if($errors->has('semester'))
                                <span class="help-block" role="alert">{{ $errors->first('semester') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.semester_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('nilai_angka') ? 'has-error' : '' }}">
                            <label for="nilai_angka">{{ trans('cruds.rekapanNilai.fields.nilai_angka') }}</label>
                            <input class="form-control" type="number" name="nilai_angka" id="nilai_angka" value="{{ old('nilai_angka', $rekapanNilai->nilai_angka) }}" step="0.01">
                            @if($errors->has('nilai_angka'))
                                <span class="help-block" role="alert">{{ $errors->first('nilai_angka') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.nilai_angka_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('nilai_huruf') ? 'has-error' : '' }}">
                            <label for="nilai_huruf">{{ trans('cruds.rekapanNilai.fields.nilai_huruf') }}</label>
                            <input class="form-control" type="text" name="nilai_huruf" id="nilai_huruf" value="{{ old('nilai_huruf', $rekapanNilai->nilai_huruf) }}">
                            @if($errors->has('nilai_huruf'))
                                <span class="help-block" role="alert">{{ $errors->first('nilai_huruf') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.nilai_huruf_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('station_diikuti') ? 'has-error' : '' }}">
                            <label for="station_diikuti">{{ trans('cruds.rekapanNilai.fields.station_diikuti') }}</label>
                            <input class="form-control" type="number" name="station_diikuti" id="station_diikuti" value="{{ old('station_diikuti', $rekapanNilai->station_diikuti) }}" step="1">
                            @if($errors->has('station_diikuti'))
                                <span class="help-block" role="alert">{{ $errors->first('station_diikuti') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.station_diikuti_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('station_tidak_lulus') ? 'has-error' : '' }}">
                            <label for="station_tidak_lulus">{{ trans('cruds.rekapanNilai.fields.station_tidak_lulus') }}</label>
                            <input class="form-control" type="number" name="station_tidak_lulus" id="station_tidak_lulus" value="{{ old('station_tidak_lulus', $rekapanNilai->station_tidak_lulus) }}" step="1">
                            @if($errors->has('station_tidak_lulus'))
                                <span class="help-block" role="alert">{{ $errors->first('station_tidak_lulus') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.station_tidak_lulus_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('ket_station_tidak_lulus') ? 'has-error' : '' }}">
                            <label for="ket_station_tidak_lulus">{{ trans('cruds.rekapanNilai.fields.ket_station_tidak_lulus') }}</label>
                            <textarea class="form-control" name="ket_station_tidak_lulus" id="ket_station_tidak_lulus">{{ old('ket_station_tidak_lulus', $rekapanNilai->ket_station_tidak_lulus) }}</textarea>
                            @if($errors->has('ket_station_tidak_lulus'))
                                <span class="help-block" role="alert">{{ $errors->first('ket_station_tidak_lulus') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.ket_station_tidak_lulus_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('ket_belum_ikut_csl') ? 'has-error' : '' }}">
                            <label for="ket_belum_ikut_csl">{{ trans('cruds.rekapanNilai.fields.ket_belum_ikut_csl') }}</label>
                            <textarea class="form-control" name="ket_belum_ikut_csl" id="ket_belum_ikut_csl">{{ old('ket_belum_ikut_csl', $rekapanNilai->ket_belum_ikut_csl) }}</textarea>
                            @if($errors->has('ket_belum_ikut_csl'))
                                <span class="help-block" role="alert">{{ $errors->first('ket_belum_ikut_csl') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.rekapanNilai.fields.ket_belum_ikut_csl_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection