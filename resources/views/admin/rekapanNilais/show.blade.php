@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.rekapanNilai.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.rekapan-nilais.index') }}">
                                Kembali
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        NIM
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->user->username ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Nama
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->user->name ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.semester') }}
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->semester ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.nilai_angka') }}
                                    </th>
                                    <td>
                                        {{ number_format($rekapanNilai->nilai_angka, 2, ',', ' ') ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.nilai_huruf') }}
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->nilai_huruf ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.station_diikuti') }}
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->station_diikuti ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.station_tidak_lulus') }}
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->station_tidak_lulus ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.ket_station_tidak_lulus') }}
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->ket_station_tidak_lulus ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.rekapanNilai.fields.ket_belum_ikut_csl') }}
                                    </th>
                                    <td>
                                        {{ $rekapanNilai->ket_belum_ikut_csl ?? '-' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.rekapan-nilais.index') }}">
                                Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
