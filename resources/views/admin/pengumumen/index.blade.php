@extends('layouts.admin')
@section('content')
<div class="content">
    @can('pengumuman_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn bg-purple" href="{{ route('admin.pengumumen.create') }}">
                    {{ trans('global.add') }} {{ trans('cruds.pengumuman.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.pengumuman.title_singular') }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table  table-bordered table-striped table-hover datatable datatable-Pengumuman">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        {{ trans('cruds.pengumuman.fields.judul') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.pengumuman.fields.isi') }}
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pengumumen as $key => $pengumuman)
                                    <tr data-entry-id="{{ $pengumuman->id }}">
                                        <td>

                                        </td>
                                        <td>
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>
                                            {{ $pengumuman->judul ?? '' }}
                                        </td>
                                        <td>
                                            {{ $pengumuman->isi ?? '' }}
                                        </td>
                                        <td>
                                            {{-- @can('pengumuman_show')
                                                <a class="btn btn-xs btn-primary" href="{{ route('admin.pengumumen.show', $pengumuman->id) }}">
                                                    {{ trans('global.view') }}
                                                </a>
                                            @endcan --}}

                                            @can('pengumuman_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.pengumumen.edit', $pengumuman->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan

                                            @can('pengumuman_delete')
                                                <form action="{{ route('admin.pengumumen.destroy', $pengumuman->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('pengumuman_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.pengumumen.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  let table = $('.datatable-Pengumuman:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

})

</script>
@endsection
