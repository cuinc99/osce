@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.pengumuman.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.pengumumen.update", [$pengumuman->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('judul') ? 'has-error' : '' }}">
                            <label class="required" for="judul">{{ trans('cruds.pengumuman.fields.judul') }}</label>
                            <input class="form-control" type="text" name="judul" id="judul" value="{{ old('judul', $pengumuman->judul) }}" required>
                            @if($errors->has('judul'))
                                <span class="help-block" role="alert">{{ $errors->first('judul') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pengumuman.fields.judul_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('isi') ? 'has-error' : '' }}">
                            <label class="required" for="isi">{{ trans('cruds.pengumuman.fields.isi') }}</label>
                            <textarea class="form-control" name="isi" id="isi" required>{{ old('isi', $pengumuman->isi) }}</textarea>
                            @if($errors->has('isi'))
                                <span class="help-block" role="alert">{{ $errors->first('isi') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.pengumuman.fields.isi_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection