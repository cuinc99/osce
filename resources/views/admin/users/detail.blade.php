@extends('layouts.admin')
@section('content')
    <div class="content">

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detail Mahasiswa
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="form-group">
                                <a class="btn btn-default" href="{{ url()->previous() }}">
                                    Kembali
                                </a>
                            </div>
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th>
                                            NIM
                                        </th>
                                        <td>
                                            {{ $user->username }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Nama Mahasiswa
                                        </th>
                                        <td>
                                            {{ $user->name }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            @foreach ($user->rekapanNilais->sortBy('semester')
        as $rekapanNilai)

                    @php
                        $array = ['success', 'info', 'primary', 'warning', 'danger'];
                        $randomColor = \HiFolks\RandoPhp\Draw::sample($array)->snap();
                    @endphp

                    <div class="col-md-3" style="margin-bottom: 30px">
                        <div class="box box-{{ $randomColor }} box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Semester {{ $rekapanNilai->semester ?? '-' }}</h3>
                            </div>
                            <div class="box-body">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Nilai Angka</b> <a
                                            class="pull-right"><span
                                                class="label bg-orange">{{ number_format($rekapanNilai->nilai_angka, 2, ',', ' ') ?? '-' }}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Nilai Huruf</b> <a
                                            class="pull-right"><span
                                                class="label bg-teal">{{ $rekapanNilai->nilai_huruf ?? '-' }}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Jumlah Station yang Diikuti</b> <a
                                            class="pull-right"><span
                                                class="label bg-maroon">{{ $rekapanNilai->station_diikuti ?? '-' }}</span></a>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <b>Jumlah Station yang Tidak Lulus</b> <a
                                            class="pull-right"><span
                                                class="label bg-aqua">{{ $rekapanNilai->station_tidak_lulus ?? '-' }}</span></a>
                                    </li>
                                </ul>

                                <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <strong>Nama Station yang Tidak Lulus</strong>

                                <p>
                                    @if ($rekapanNilai->ket_station_tidak_lulus)
                                        @php
                                            $ketstl = explode(',', $rekapanNilai->ket_station_tidak_lulus);
                                        @endphp
                                        @foreach ($ketstl as $ket)
                                            <span class="label label-danger">{{ $ket }}</span>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </p>

                                <hr>

                                <i class="fa fa-chevron-circle-right text-{{ $randomColor }} "></i> <strong>Nama Station yang Belum Ikut CSL
                                </strong>

                                <p>
                                    @if ($rekapanNilai->ket_belum_ikut_csl)
                                        @php
                                            $ketsbl = explode(',', $rekapanNilai->ket_belum_ikut_csl);
                                        @endphp
                                        @foreach ($ketsbl as $ket)
                                            <span class="label label-warning">{{ $ket }}</span>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </p>

                                <hr>

                                @comments(['model' => $rekapanNilai])
                            </div>
                        </div>
                    </div>

                @endforeach

        </div>

    </div>
@endsection
