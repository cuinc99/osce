@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Komentar yang belum dibaca
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10">
                                        No
                                    </th>
                                    <th>
                                        Pengirim
                                    </th>
                                    <th>
                                        Rekapan Nilai
                                    </th>
                                    <th width="40%">
                                        Pesan
                                    </th>
                                    <th width="20%">

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($comments as $comment)
                                <tr>
                                    <td>
                                        {{ $loop->iteration }}
                                    </td>
                                    <td>
                                        <strong>{{ $comment->commenter->username }}</strong> -
                                        {{ $comment->commenter->name }}

                                    </td>
                                    <td>
                                        Semester {{ $comment->commentable->semester }}
                                    </td>
                                    <td>
                                        {{ $comment->comment }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.mahasiswa.show', $comment->commenter->id) }}" class="btn btn-primary">Lihat Komentar</a>
                                        <a href="{{ route('admin.komentar.read', $comment->id) }}" class="btn btn-success">Tandai Sudah Bibaca</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">
                                        Belum ada komentar
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
