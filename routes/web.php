<?php

use App\Models\User;
use App\Models\Comment;


Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Rekapan Nilai
    Route::delete('rekapan-nilais/destroy', 'RekapanNilaiController@massDestroy')->name('rekapan-nilais.massDestroy');
    Route::post('rekapan-nilais/import', 'RekapanNilaiController@import')->name('rekapan-nilais.import');
    Route::resource('rekapan-nilais', 'RekapanNilaiController');

    // Pengumuman
    Route::delete('pengumumen/destroy', 'PengumumanController@massDestroy')->name('pengumumen.massDestroy');
    Route::resource('pengumumen', 'PengumumanController');

    Route::get('mahasiswa/{user}', function (User $user) {
        return view('admin.users.detail', compact('user'));
    })->name('mahasiswa.show');

    Route::get('komentar', function () {
        $comments = Comment::where('commenter_id', '!=', auth()->id())->where('read', false)->get();
        return view('admin.komentar', compact('comments'));
    })->name('komentar');

    Route::get('komentar/{id}/read', function (Comment $id) {
        $id->update(['read' => true]);
        return back();
    })->name('komentar.read');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});

Route::get('app-auth/login-by/{id}', function ($id) {
    Auth::loginUsingId($id);

    return redirect()->route('admin.home');
});

Route::get('login-by/{username}', function ($username) {
    $user = User::where('username', $username)->first();

    if (!$user) {
        return 'Maaf! akun tidak ditemukan.';
    }

    Auth::loginUsingId($user->id);

    return redirect()->route('admin.home');
});
