<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::post('app-auth', function (Request $request) {

    $data = [
        'username' => $request->username,
        'password' => $request->password,
    ];

    if (Auth::attempt($data)) {
        return auth()->id();
    } else {
        return 'n';
    }
});
