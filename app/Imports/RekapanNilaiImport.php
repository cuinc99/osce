<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Str;
use App\Models\RekapanNilai;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class RekapanNilaiImport implements ToCollection, WithStartRow
{
    public function collection(Collection $rows)
    {
        $import_code = Str::random(10);

        foreach ($rows as $row)
        {
            if ($row[0] == 'NO') {
                return NULL;
            }

            $user = User::where('username', $row[1])->first();

            if (! $user) {
                $user = User::create([
                    'username' => $row[1],
                    'name' => $row[2],
                    'password' => bcrypt('password'),
                ]);
                $user->roles()->sync(2);
            }

            RekapanNilai::create([
                'semester' => $row[3],
                'nilai_angka' => $row[4],
                'nilai_huruf' => $row[5],
                'station_diikuti' => $row[6],
                'station_tidak_lulus' => $row[7],
                'ket_station_tidak_lulus' => $row[8],
                'ket_belum_ikut_csl' => $row[9],
                'import_code' => $import_code,
                'user_id' => $user->id,
            ]);
        }
    }

    public function startRow(): int
    {
        return 3;
    }
}
