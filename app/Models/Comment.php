<?php

namespace App\Models;

use Laravelista\Comments\Comment as LaravelistaComment;

class Comment extends LaravelistaComment
{
    protected $fillable = [
        'read'
    ];
}
