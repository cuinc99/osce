<?php

namespace App\Models;

use \DateTimeInterface;
use Laravelista\Comments\Commentable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RekapanNilai extends Model
{
    use HasFactory;
    use Commentable;

    public $table = 'rekapan_nilai';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'semester',
        'nilai_angka',
        'nilai_huruf',
        'station_diikuti',
        'station_tidak_lulus',
        'ket_station_tidak_lulus',
        'ket_belum_ikut_csl',
        'import_code',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
