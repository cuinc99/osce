<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Pengumuman;
use App\Models\RekapanNilai;

class HomeController
{
    public function index()
    {
        $pengumumans = Pengumuman::latest()->get();
        $mahasiswas = User::with('roles')->whereHas('roles', fn($q) => ($q->where('id', 2)))->get();
        $rekapanNilaiCount = RekapanNilai::count();

        return view('home', compact('mahasiswas', 'rekapanNilaiCount', 'pengumumans'));
    }
}
