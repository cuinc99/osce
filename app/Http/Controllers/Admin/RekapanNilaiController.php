<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\Models\RekapanNilai;
use Illuminate\Http\Request;
use App\Imports\RekapanNilaiImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\StoreRekapanNilaiRequest;
use App\Http\Requests\UpdateRekapanNilaiRequest;
use App\Http\Requests\MassDestroyRekapanNilaiRequest;

class RekapanNilaiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('rekapan_nilai_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rekapanNilais = RekapanNilai::latest()->get();

        return view('admin.rekapanNilais.index', compact('rekapanNilais'));
    }

    public function import(Request $request)
    {
        $this->validate($request, [
			'file' => 'required|mimes:xls,xlsx'
		]);
        Excel::import(new RekapanNilaiImport, $request->file('file'));
        return back();
    }

    public function create()
    {
        abort_if(Gate::denies('rekapan_nilai_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rekapanNilais.create');
    }

    public function store(StoreRekapanNilaiRequest $request)
    {
        $rekapanNilai = RekapanNilai::create($request->all());

        return redirect()->route('admin.rekapan-nilais.index');
    }

    public function edit(RekapanNilai $rekapanNilai)
    {
        abort_if(Gate::denies('rekapan_nilai_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rekapanNilais.edit', compact('rekapanNilai'));
    }

    public function update(UpdateRekapanNilaiRequest $request, RekapanNilai $rekapanNilai)
    {
        $rekapanNilai->update($request->all());

        return redirect()->route('admin.rekapan-nilais.index');
    }

    public function show(RekapanNilai $rekapanNilai)
    {
        abort_if(Gate::denies('rekapan_nilai_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rekapanNilais.show', compact('rekapanNilai'));
    }

    public function destroy(RekapanNilai $rekapanNilai)
    {
        abort_if(Gate::denies('rekapan_nilai_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rekapanNilai->delete();

        return back();
    }

    public function massDestroy(MassDestroyRekapanNilaiRequest $request)
    {
        RekapanNilai::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
