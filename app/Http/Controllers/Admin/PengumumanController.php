<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPengumumanRequest;
use App\Http\Requests\StorePengumumanRequest;
use App\Http\Requests\UpdatePengumumanRequest;
use App\Models\Pengumuman;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PengumumanController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('pengumuman_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pengumumen = Pengumuman::all();

        return view('admin.pengumumen.index', compact('pengumumen'));
    }

    public function create()
    {
        abort_if(Gate::denies('pengumuman_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pengumumen.create');
    }

    public function store(StorePengumumanRequest $request)
    {
        $pengumuman = Pengumuman::create($request->all());

        return redirect()->route('admin.pengumumen.index');
    }

    public function edit(Pengumuman $pengumuman)
    {
        abort_if(Gate::denies('pengumuman_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pengumumen.edit', compact('pengumuman'));
    }

    public function update(UpdatePengumumanRequest $request, Pengumuman $pengumuman)
    {
        $pengumuman->update($request->all());

        return redirect()->route('admin.pengumumen.index');
    }

    public function show(Pengumuman $pengumuman)
    {
        abort_if(Gate::denies('pengumuman_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pengumumen.show', compact('pengumuman'));
    }

    public function destroy(Pengumuman $pengumuman)
    {
        abort_if(Gate::denies('pengumuman_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pengumuman->delete();

        return back();
    }

    public function massDestroy(MassDestroyPengumumanRequest $request)
    {
        Pengumuman::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
