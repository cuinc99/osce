<?php

namespace App\Http\Requests;

use App\Models\RekapanNilai;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRekapanNilaiRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('rekapan_nilai_edit');
    }

    public function rules()
    {
        return [
            'semester' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'nilai_angka' => [
                'numeric',
            ],
            'nilai_huruf' => [
                'string',
                'nullable',
            ],
            'station_diikuti' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'station_tidak_lulus' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
