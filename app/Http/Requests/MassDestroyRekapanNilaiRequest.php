<?php

namespace App\Http\Requests;

use App\Models\RekapanNilai;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRekapanNilaiRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rekapan_nilai_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:rekapan_nilais,id',
        ];
    }
}
