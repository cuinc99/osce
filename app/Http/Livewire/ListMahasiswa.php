<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class ListMahasiswa extends Component
{
    use WithPagination;

    public $cari = "";

    public function render()
    {
        $mahasiswas = User::query()
            ->with('roles')
            ->whereHas('roles', fn($q) => ($q->where('id', 2)))
            ->when($this->cari != "", function($q) {
                $q->where('username', 'like', '%' . $this->cari . '%');
                $q->orWhere('name', 'like', '%' . $this->cari . '%');
            })
            ->paginate(20);

        return view('livewire.list-mahasiswa', compact('mahasiswas'));
    }
}
